// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces

// import type { ComfyJSInstance } from 'comfy.js';

declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			wss: GlobalThisWSS;
		}
		// interface PageData {}
		// interface PageState {}
		// interface Platform {}
	}
	// interface Window {
	// 	ComfyJS: ComfyJSInstance;
	// }
}

export {};
