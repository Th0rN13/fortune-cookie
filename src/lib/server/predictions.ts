import { readFileSync } from 'node:fs';

let predictions: Array<string> = [];

export function getPredictions() {
	if (!predictions.length) {
		const buff = readFileSync('./data/quotes.txt');
		predictions = buff
			.toString()
			.split('\n')
			.map((str: string) => str.trim())
			.filter((str) => str);
	}
	predictions.push('');
	return predictions;
}

export function getRandomPrediction(): string {
	if (!predictions.length) getPredictions();
	const idx = Math.floor(Math.random() * predictions.length);
	return predictions[idx];
}
