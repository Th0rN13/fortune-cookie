import ComfyJS from 'comfy.js';
import { GlobalThisWSS } from '$lib/server/webSocketUtils';
import type { ExtendedGlobal } from '$lib/server/webSocketUtils';
import { getRandomPrediction } from './predictions';

// =======================

let wssInitialized = false;
export const startupWebsocketServer = () => {
	if (wssInitialized) return;
	console.log('[wss:kit] setup');
	const wss = (globalThis as ExtendedGlobal)[GlobalThisWSS];
	if (wss !== undefined) {
		wss.on('connection', (ws) => {
			// This is where you can authenticate the client from the request
			// const session = await getSessionFromCookie(request.headers.cookie || '');
			// if (!session) ws.close(1008, 'User not authenticated');
			// ws.userId = session.userId;
			console.log(`[wss:kit] client connected (${ws.socketId})`);

			ws.on('message', (e: Buffer) => {
				console.log('msg', e.toString());
				const reward: rewardFull | undefined = generateReward(
					'Frontend',
					Math.random().toString(),
					new Date().toString()
				);
				wssSend(JSON.stringify(reward));
			});

			ws.on('close', () => {
				console.log(`[wss:kit] client disconnected (${ws.socketId})`);
			});
		});
		wssInitialized = true;
	}
};

function wssSend(msg: string) {
	const wss = (globalThis as ExtendedGlobal)[GlobalThisWSS];
	wss.clients.forEach((ws) => {
		ws.send(msg);
	});
}

// =======================

const fortuneCookieRewardId = '00c4def8-97a9-471d-90fe-13486a37f778';

interface rewardFull {
	user: string;
	timestamp: string;
	text: string;
	id: string;
	sended?: boolean;
}

const rewardsQueue: Record<string, rewardFull> = {};

function clearOldRewards() {
	const curTime = new Date();
	for (const key of Object.keys(rewardsQueue)) {
		const rewardInQueue = rewardsQueue[key];
		const time = new Date(rewardInQueue.timestamp);
		const sended = rewardInQueue.sended;
		if (curTime.getTime() - time.getTime() > 10 * 60 * 1000 && sended) {
			// 10 minutes
			delete rewardsQueue[key];
		}
	}
}

function generateReward(user: string, id: string, timestamp: string): rewardFull | undefined {
	clearOldRewards();
	const text = getRandomPrediction();
	console.log('reward:', user, id, timestamp);
	const currentReward: rewardFull = {
		user,
		text,
		timestamp,
		id
	};
	if (rewardsQueue[id]) return undefined;
	rewardsQueue[id] = currentReward;
	return currentReward;
}

async function onReward(
	user: string,
	_reward: unknown,
	cost: unknown,
	message: unknown,
	extra: {
		customRewardId?: string;
		timestamp?: string;
		reward?: {
			id?: string;
		};
	}
) {
	if (extra?.reward?.id !== fortuneCookieRewardId) return;
	const id = extra?.customRewardId ?? 'empty';
	const timestamp = extra?.timestamp ?? 'empty';
	const reward: rewardFull | undefined = generateReward(user, id, timestamp);
	if (!reward) return;
	let chatMsg = `${reward.user} куснул өчпочмак, внутри оказалась газета "Агрызский вестник", но без букв`;
	if (reward.text) {
		chatMsg = `${reward.user} куснул өчпочмак, внутри оказалась газета "Агрызский вестник" с надписью: "${reward.text}"`;
	}
	wssSend(JSON.stringify(reward));
	setTimeout(() => {
		// @ts-expect-error Don't need channel
		ComfyJS.Say(chatMsg);
	}, 1000);
}

ComfyJS.onReward = onReward;

ComfyJS.Init('Sapa_Evil', 'oauth:ei0i7ynlggdv6rr3aw4ath0gl27o57');
