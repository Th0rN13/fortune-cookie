import { getPredictions } from '$lib/server/predictions';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async () => {
	const strings = getPredictions();
	return json(strings);
};
